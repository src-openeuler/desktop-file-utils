Name:          desktop-file-utils
Version:       0.28
Release:       1
Summary:       Desktop-file-utils contains utilities for manipulating desktop entries
License:       GPL-2.0-or-later
URL:           https://www.freedesktop.org/wiki/Software/desktop-file-utils/
Source0:       https://www.freedesktop.org/software/desktop-file-utils/releases/%{name}-%{version}.tar.xz
Source1:       desktop-entry-mode-init.el

BuildRequires: gcc
BuildRequires: meson >= 0.61.0
BuildRequires: pkgconfig(gio-2.0) >= 2.26
BuildRequires: pkgconfig(glib-2.0) >= 2.26
BuildRequires: rpm_macro(_emacs_sitelispdir)
Requires:      emacs-filesystem
Provides:      emacs-%{name} = %{version}-%{release} emacs-%{name}-el = %{version}-%{release}
Obsoletes:     emacs-%{name} < 0.20-3 emacs-%{name}-el < 0.20-3

%description
desktop-file-utils contains command line utilities for working with desktop entries, it contains:
desktop-file-validate:
   validates a desktop file and prints warnings/errors about desktop entry specification violations.
desktop-file-install:
   installs a desktop file to the applications directory, optionally munging it a bit in transit.
update-desktop-database:
   updates the database containing a cache of MIME types handled by desktop files.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson
%meson_build

%install
%meson_install

install -d $RPM_BUILD_ROOT%{_emacs_sitelispdir}/%{name}
mv $RPM_BUILD_ROOT%{_emacs_sitelispdir}/*.el* $RPM_BUILD_ROOT%{_emacs_sitelispdir}/%{name}
install -Dpm 644 %{S:1} $RPM_BUILD_ROOT%{_emacs_sitestartdir}/desktop-entry-mode-init.el
touch $RPM_BUILD_ROOT%{_emacs_sitestartdir}/desktop-entry-mode-init.elc

%transfiletriggerin -- %{_datadir}/applications
update-desktop-database &> /dev/null || :

%transfiletriggerpostun -- %{_datadir}/applications
update-desktop-database &> /dev/null || :

%files
%doc AUTHORS
%license COPYING
%{_bindir}/*
%{_emacs_sitelispdir}/%{name}
%{_emacs_sitestartdir}/desktop-entry-mode-init.el
%ghost %{_emacs_sitestartdir}/desktop-entry-mode-init.elc

%files help
%doc README NEWS
%{_mandir}/man1/*

%changelog
* Sun Oct 27 2024 Funda Wang <fundawang@yeah.net> - 0.28-1
- update to version 0.28

* Mon Aug 19 2024 Chunchun Yang <yangchunchun@cqsoftware.com.cn> - 0.27-3
- Replaced declaration of subpackage with the 'package_help' macro.

* Fri Jan 26 2024 yinhongchang <yinhongchang@kylinsec.com.cn> - 0.27-2
- KYOS-B: Add trigger to update-desktop-database desktop

* Fri Dec 29 2023 Paul Thomas <paulthomas100199@gmail.com> - 0.27-1
- update to version 0.27

* Thu Sep 08 2022 misaka00251 <misaka00251@misakanet.cn> - 0.26-3
- Support files conforming to 1.5 of the specification

* Tue May 18 2021 lin.zhang <lin.zhang@turbolinux.com.cn>  - 0.26-2
- add BuildRequires gcc-c++

* Mon Jul 20 2020 wangye <wangye70@huawei.com> - 0.26-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade to 0.26

* Thu Jan 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.24-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade to 0.24

* Mon Sep 9 2019 yanzhihua <yanzhihua4@huawei.com> - 0.23-10
- Package init

